swapon --show
sudo swapoff -a
sudo fallocate -l 8G /swapfile
ls -lh /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
free -h
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
