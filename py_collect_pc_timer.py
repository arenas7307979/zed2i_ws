# coding=utf-8

import tkinter
import time

f= open("ori_timer_stamp.txt","x+")

def record_timestamp():
	print(str(time.time()))
	f.write(str(time.time()))
	f.write('\n')

count = 1
def show(aa):
    def counting():
        global count
        count += 1
        aa.config(text=str(time.time()))
        aa.after(1000, counting)
    counting()



root = tkinter.Tk()
label = tkinter.Label(root, bg="yellow", fg="red", height=3, width=20)
buttonRecord = tkinter.Button(root, text="record_time", width=15, command = record_timestamp)
buttonBye = tkinter.Button(root, text="exit", width=15, command=root.destroy)
label.pack()
show(label)
buttonRecord.pack(side=tkinter.LEFT)
buttonBye.pack(side=tkinter.LEFT)
root.mainloop()

