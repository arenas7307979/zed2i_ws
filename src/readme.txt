
===============================zed啟動相機
1.Under Path of cd zed2i_ws/
2.link lib : source devel/setup.bash
3.roslaunch zed_wrapper zed2i.launch 


===============================當出現bug : ZED connection -> CAMERA FAILED TO SETUP
1. 嘗試更新網站zed最新的驅動

===============================安裝zed驅動 

1. cd basic_tool/
   Install ZED_driver: ./ZED_SDK_Ubuntu18_cuda10.0_v3.6.5.run -- silent

3. using boy7302013/libslam_cuda10_for_zed2i docker 

4. 
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release -DQt5_DIR:PATH=/opt/Qt/5.9.9/gcc_64/lib/cmake/Qt5
catkin config --merge-devel

5. catkin clean

6. catkin build 可能第一次會失敗, 由於ros-sensor_msgs_ext 尚未安裝（第一次）

7. catkin build 在一次嘗試catkin build 完成編譯

8. "校正磁力計", zed2i ouput : uT

9. cd /

10. ZED_Sensor_Viewer (show mag value API)

11. https://www.stereolabs.com/docs/sensors/magnetometer/#magnetometer-calibration
    初始化平面XY




<backup_file>
===========================================
FROM nvidia/cuda:10.2-base-ubuntu18.04

ENV NVIDIA_DRIVER_CAPABILITIES compute,video,utility

# Setup the ZED SDK
RUN apt-get update -y && apt-get autoremove -y && \
    apt-get install --no-install-recommends wget curl libusb-1.0-0 curl libhidapi-libusb0 libopenblas-base libjpeg-turbo8 libturbojpeg libpng16-16 udev libgomp1 -y && \
    apt-get install --no-install-recommends cuda-npp-10-2 -y && \
    wget -O ZED_SDK_Linux_Ubuntu18.run https://download.stereolabs.com/zedsdk/2.8/cu102/ubuntu18 && \
    chmod +x ZED_SDK_Linux_Ubuntu18.run ; ./ZED_SDK_Linux_Ubuntu18.run  --noexec --target zed_installer && \
    rm ZED_SDK_Linux_Ubuntu18.run && \
    mkdir -p /usr/local/zed/lib ; mkdir -p /usr/local/zed/settings  ; mv zed_installer/lib/*.so /usr/local/zed/lib && \
    mv "zed_installer/99-slabs.rules" "/etc/udev/rules.d/"; chmod 777 "/etc/udev/rules.d/99-slabs.rules" && \
    mv "zed_installer/zed.conf" "/etc/ld.so.conf.d/" ; ldconfig ; usermod -a -G video "$(whoami)" && \
    rm -rf zed_installer && \
    rm -rf /var/lib/apt/lists/*

============================================
# This script will setup USB rules to open the ZED cameras without root access
# This can also be useful to access the cameras from a docker container without root (this script needs to be run on the host)
# NB: Running the ZED SDK installer will already setup those

# Print the commands
set -x
# Download the lightest installer
wget -q https://download.stereolabs.com/zedsdk/3.5/jp44/jetsons -O zed_installer.run
# Extracting only the file we're interested in
bash ./zed_installer.run --tar -x './99-slabs.rules'  > /dev/null 2>&1
sudo mv "./99-slabs.rules" "/etc/udev/rules.d/"
sudo chmod 777 "/etc/udev/rules.d/99-slabs.rules"
sudo udevadm control --reload-rules && sudo udevadm trigger

============================================



===============================zed_錄製
[@@record Gray imu/mag/stereoimages]
rosbag record -b 2048 /zed2i/zed_node/left_raw/image_raw_gray /zed2i/zed_node/right_raw/image_raw_gray /zed2i/zed_node/imu/mag /zed2i/zed_node/imu/data_raw /zed2i/zed_node/imu/data

[@@record RGB imu/mag/stereoimages]
rosbag record -b 2048 /zed2i/zed_node/left_raw/image_raw_color /zed2i/zed_node/right_raw/image_raw_color /zed2i/zed_node/imu/mag /zed2i/zed_node/imu/data_raw /zed2i/zed_node/imu/data

[record imu/mag only]
rosbag record -b 2048 /zed2i/zed_node/imu/mag /zed2i/zed_node/imu/data_raw /zed2i/zed_node/imu/data


===============================查詢zed2i校正參數,之後用於orbslam3[啟動roslaunch zed_wrapper zed2i.launch]
[左眼與imu外參, leftcam_to_imu-->Tbc]
rostopic echo /zed2i/zed_node/left_cam_imu_transform
[左眼/右眼相機參數]
rostopic echo /zed2i/zed_node/left_raw/camera_info
rostopic echo /zed2i/zed_node/right_raw/camera_info


