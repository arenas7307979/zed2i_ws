xhost +local:root

XAUTH=/tmp/.docker.xauth
if [ ! -f $XAUTH ]
then
    xauth_list=$(xauth nlist :0 | sed -e 's/^..../ffff/')
    if [ ! -z "$xauth_list" ]
    then
        echo $xauth_list | xauth -f $XAUTH nmerge -
    else
        touch $XAUTH
    fi
    chmod a+r $XAUTH
fi

docker run \
    --privileged \
    --gpus all \
    -v /home/xunwei/docker_slam/datasets:/datasets \
    -v /home/xunwei/docker_slam/our_nas:/our_nas \
    -v /home/xunwei/docker_slam/android_develop:/android_develop \
    -v /home/xunwei/docker_slam/slam_core:/slam_core \
    -v /home/xunwei/docker_slam/slam_course:/slam_course \
    -v /home/xunwei/docker_slam/kalibr_ws:/kalibr_ws \
    -v /sys/kernel/debug:/sys/kernel/debug \
    -v /dev:/dev \
    -it \
    --env="DISPLAY" \
    --device=/dev:/dev \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --env="XAUTHORITY=$XAUTH" \
    --volume="$XAUTH:$XAUTH" \
    --runtime=nvidia \
    boy7302013/libslam_cuda10_for_zed2i \
    bash
